/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/* peripherals */
/* LED to indicate scale reading
 * pin: PB5, LED L
 */
#define LED_PORT PORTB
#define LED_DDR DDRB
#define LED_PIN PINB
#define LED_IO PB5
/* switch to detect if fridge is open
 * pin: PD3, PCINT19, D3
 */
#define FRIDGE_PORT PORTD
#define FRIDGE_DDR DDRD
#define FRIDGE_PIN PIND
#define FRIDGE_IO PD3
#define FRIDGE_PCINT PCINT19
/* PIR motion detector
 * pin: PD4, PCINT20, D4
 */
#define MOTION_PORT PORTD
#define MOTION_DDR DDRD
#define MOTION_PIN PIND
#define MOTION_IO PD4
#define MOTION_PCINT PCINT20
/* light barrier power control
 * pin: PD5, D5
 */
#define LIGHT_PORT PORTD
#define LIGHT_DDR DDRD
#define LIGHT_PIN PIND
#define LIGHT_IO PD5
/* light barrier status
 * pin: PD6, D6
 */
#define BARRIER_PORT PORTD
#define BARRIER_DDR DDRD
#define BARRIER_PIN PIND
#define BARRIER_IO PD6
#define BARRIER_PCINT PCINT22

