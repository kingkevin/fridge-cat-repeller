# Arduino (Nano) with ATmega328P programming script
# detailed configuration on http://arduino.cc/en/uploads/Main/boards.txt
# required packages: avrdude gcc-avr avr-libc

# the output firmware name
TARGET = firmware

# the target (arduino nano)
DEVICE = atmega328p
F_CPU = 16000000UL
# the flasher
PROGRAMMER = arduino
PORT = /dev/ttyUSB0
# to flash
# use the aduino bootlaoder, with a baudrate of 57600
# reset the device to start bootlaoder
FLASHER = avrdude -p $(DEVICE) -c $(PROGRAMMER) -P $(PORT) -b 57600 -D

# compiler executables
CC = avr-gcc
OBJDUMP = avr-objdump
OBJCOPY = avr-objcopy
SIZE = avr-size

# library directories, compiler, and linker flags
LIBS = lib
CFLAGS = -g -Wall -Werror -O3 -std=c99
CFLAGS += -I. $(patsubst %,-I%,$(LIBS))
CFLAGS += -mmcu=$(DEVICE) -DF_CPU=$(F_CPU)
LDFLAGS = -Wl,-Map=$(TARGET).map,--cref
LDFLAGS += -I. $(patsubst %,-I%,$(LIBS))
LDFLAGS += -mmcu=$(DEVICE)
# floating point printf version (requires -lm below)
#LDFLAGS += -Wl,-u,vfprintf -lprintf_flt
# math library
#LDFLAGS += -lm

# source files to compile
SRC = $(wildcard *.c)
SRC += $(foreach LIB,$(LIBS),$(wildcard $(LIB)/*.c))
# header files
HEADER = $(SRC:.c=.h)
# object files
OBJ = $(SRC:.c=.o)
# listing files.
LST = $(SRC:.c=.lst)

all: compile flash
	$(info EEPROM has to be programmed separately)

debug: CFLAGS += -DDEBUG
debug: map lst all

# reset board by setting DTR
# the capacitor on DTR with create a pulse on RESET
# after reset the bootloader is start
# the bootloader can be used to reflash the device
reset:
	stty 57600 raw ignbrk hup < $(PORT)

# flash the device using the internal bootloader
flash: $(TARGET).hex reset
	$(FLASHER) -U flash:w:$<:i

# write EEPROM on the device
eeprom: $(TARGET)_eeprom.hex reset
	$(FLASHER) -U eeprom:w:$<:i

# write bootloader and fuses
bootloader:
	wget http://code.google.com/p/arduino/source/browse/trunk/hardware/arduino/bootloaders/atmega/ATmegaBOOT_168_atmega328.hex
	avrdude -p $(DEVICE) -c usbtiny	-U lfuse:w:0xff:m -U hfuse:w:0xda:m -U efuse:w:0x05:m -U flash:w:ATmegaBOOT_168_atmega328.hex:i
	rm ATmegaBOOT_168_atmega328.hex

# create main target firmware
compile: $(TARGET).elf
	$(SIZE) --format=avr --mcu=$(DEVICE) $(TARGET).elf

# C + ASM file
lst: $(TARGET).lst

# contains global and static variables
map: $(TARGET).map

# compile source files
%.o: %.c %.h
	$(CC) $(CFLAGS) -c -o $@ $<

# link compiled files
%.elf: $(OBJ)
	$(CC) $(LDFLAGS) -o $@ $^

$(TARGET).map: $(OBJ)
	$(CC) $(LDFLAGS) -Wl,-Map=$@,--cref -o /dev/null $^

# create extended listing for additional information
%.lst: %.elf
	$(OBJDUMP) --section-headers --source $< > $@

# create flashable ihex from firmware
%.hex: %.elf
	$(OBJCOPY) --only-section .text --only-section .data --output-target ihex $< $@

%_eeprom.hex: %.elf
	$(OBJCOPY) --only-section .eeprom --change-section-lma .eeprom=0 --output-target ihex $< $@

clean:
	rm -f $(TARGET) $(TARGET).hex $(TARGET)_eeprom.hex $(TARGET).elf $(TARGET).lst $(TARGET).map $(LST) $(OBJ)
