/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This library handles the Two-Wire Interface (TWI)/Inter-Integrated Circuit (I²C) configuration
 * based on demo by Ruwan Jayanetti from AVR libc examples
 * http://www.nongnu.org/avr-libc/user-manual/group__twi__demo.html
 */
#include <stdint.h> // Standard Integer Types
#include <stdlib.h> // General utilities
#include <stdbool.h> // Boolean

#include <avr/io.h>  // AVR device-specific IO definitions
#include <util/twi.h> // 2-wire TWSR values

#include <twi.h> // definitions

/* initialize 2-wire interface */
void twi_init(void)
{
	TWSR &= ~((1<<TWPS0)|(1<<TWPS1)); // set TWI prescaler to 1X
#if F_CPU < 3600000UL
	TWBR = 10; // smallest TWBR value
#else
	TWBR = (uint8_t)((F_CPU/F_TWI-16)/2); // set TWI clock speed
#endif
}

/* read <len> bytes inyo <buf> using 2-wire interface, starting at <addr>
 * returns if it succeeded
 * 16 bits address is not supported
 */
bool twi_read_bytes(uint8_t addr, uint8_t len, uint8_t *buf)
{
	uint16_t retry = 0; // number of retries
	bool rc = false; // return code

	/* number of times to retry */
restart:
	if (retry++ >= MAX_ITER) {
		return false;
	}

	/* first cycle: master transmitter mode */
start:

	/* send start */
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN); // send start condition
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_REP_START: // OK, but should not happen
		case TW_START:
			break;
		case TW_MT_ARB_LOST: // re-arbitrate
			goto start;
		default:
			return -1;
	}

	/* send SLA+W */
	TWDR = SLA | TW_WRITE;
	TWCR = (1<<TWINT) | (1<<TWEN); // clear interrupt to start transmission
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_MT_SLA_ACK:
			break;
		case TW_MT_SLA_NACK: // nack during select: device busy writing
			goto restart;
		case TW_MT_ARB_LOST: // re-arbitrate
			goto start;
		default:
			goto stop; // must send stop condition
	}

	/* send memory address */
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN); // clear interrupt to start transmission
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_MT_DATA_ACK:
			break;
		case TW_MT_DATA_NACK:
			goto stop;
		case TW_MT_ARB_LOST:
			goto start;
		default:
			goto stop; // must send stop condition
	}

	/* next cycle(s): master receiver mode */

	/* repeat start */
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN); // send start condition
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_START: // OK, but should not happen
		case TW_REP_START:
			break;
		case TW_MT_ARB_LOST:
			goto start;
		default:
			goto stop;
	}

	/* send SLA+R */
	TWDR = SLA | TW_READ;
	TWCR = (1<<TWINT) | (1<<TWEN); // clear interrupt to start transmission
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_MR_SLA_ACK:
			break;
		case TW_MR_SLA_NACK:
			goto stop;
		case TW_MR_ARB_LOST:
			goto start;
		default:
			goto stop;
	}

	/* read bytes */
	while (len > 0) {
		if (len > 1) {
			TWCR = (1<<TWINT) | (1<<TWEN) | (1<<TWEA); // send ACK
		} else { // last byte
			TWCR = (1<<TWINT) | (1<<TWEN); // send NACK
		}
		while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
		switch (TW_STATUS) {
			case TW_MR_DATA_NACK: // last byte received
				len = 1; // force end of loop (save as in next case)
			case TW_MR_DATA_ACK:
				*buf++ = TWDR;
				len--;
				break;
			default:
				goto stop;
		}
	}
	rc = true;

	/* send stop */
stop:
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN); // send stop condition
	return rc;
}

/* write <len> bytes from <buf> on 2-wire interface starting at <addr>
 * returns if it successed
 * 16 bits address is not supported
 * ensure yourself the size of pages to be written
 */
bool twi_write_bytes(uint8_t addr, uint8_t len, uint8_t *buf)
{
	uint16_t retry = 0; // number of retries
	bool rc = false; // return code

	/* number of times to retry */
restart:
	if (retry++ >= MAX_ITER) {
		return false;
	}

	/* master transmit mode */
start:

	/* start */
	TWCR = (1<<TWINT) | (1<<TWSTA) | (1<<TWEN); // send start condition
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_REP_START: // OK, but should not happen
		case TW_START:
			break;
		case TW_MT_ARB_LOST:
			goto start;
		default:
			return false; // error: not in start condition
	}

	/* send SLA+W */
	TWDR = SLA | TW_WRITE;
	TWCR = (1<<TWINT) | (1<<TWEN); // clear interrupt to start transmission
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_MT_SLA_ACK:
			break;
		case TW_MT_SLA_NACK: // nack during select: device busy writing
			goto restart;
		case TW_MT_ARB_LOST: // re-arbitrate
			goto start;
		default:
			goto stop; // must send stop condition
	}

	/* send address */
	TWDR = addr;
	TWCR = (1<<TWINT) | (1<<TWEN); // clear interrupt to start transmission
	while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
	switch (TW_STATUS) {
		case TW_MT_DATA_ACK:
			break;
		case TW_MT_DATA_NACK:
			goto stop;
		case TW_MT_ARB_LOST:
			goto start;
		default:
			goto stop;
	}

	/* write bytes */
	for (; len > 0; len--) {
		TWDR = *buf++;
		TWCR = (1<<TWINT) | (1<<TWEN); // start transmission
		while ((TWCR & (1<<TWINT)) == 0); // wait for transmission
		switch (TW_STATUS) {
			case TW_MT_DATA_NACK:  // device write protected
				goto stop;
			case TW_MT_DATA_ACK:
				break;
			default:
				goto stop;
		}
	}
	rc = true;

	/* send stop */
stop:
	TWCR = (1<<TWINT) | (1<<TWSTO) | (1<<TWEN); // send stop condition
	return rc;
}
