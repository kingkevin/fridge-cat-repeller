/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This library handles the USART configuration */

#include <stdint.h> /* Standard Integer Types */
#include <stdio.h> /* Standard IO facilities */
#include <stdlib.h> /* General utilities */
#include <avr/io.h> /* AVR device-specific IO definitions */

#include "uart.h" /* UART header */
#include <util/setbaud.h> /* Helper macros for baud rate calculations */

/* assign input and output streams */
FILE uart_output = FDEV_SETUP_STREAM((int (*)(char, struct __file *)) uart_putchar, NULL, _FDEV_SETUP_WRITE);
FILE uart_input = FDEV_SETUP_STREAM(NULL, (int (*)(struct __file *)) uart_getchar, _FDEV_SETUP_READ);
FILE uart_io = FDEV_SETUP_STREAM((int (*)(char, struct __file *)) uart_putchar, (int (*)(struct __file *)) uart_getchar, _FDEV_SETUP_RW);

/* configure serial port */
void uart_init(void)
{
	UBRR0H = UBRRH_VALUE; // set baurate (calculated from BAUD)
	UBRR0L = UBRRL_VALUE; // set baurate (calculated from BAUD)
#if USE_2X
	UCSR0A |= (1<<U2X0); // use double speed (set depending on BAUD)
#else
	UCSR0A &= ~(1<<U2X0); // do not use double speed (set depending on BAUD)
#endif
	UCSR0C = (1<<UCSZ01)|(1<<UCSZ00); // 8N1 bit data
	UCSR0B = (1<<RXEN0)|(1<<TXEN0);   // enable RX and TX
	UCSR0B |= (1<<RXCIE0); // enable RX complete interrupt
}

/* put character on UART stream */
void uart_putchar(char c, FILE *stream)
{
	if (c == '\n') { // add carrier return before line feed. this is recommended for most UART terminals
		uart_putchar('\r', stream);
	}
	while (!(UCSR0A&(1<<UDRE0))); // wait for transmit register to empty
	UDR0 = c; // put data in transmit register
	while (!(UCSR0A&(1<<TXC0))); // wait for transmission to complete (else sleep mode can break it)
	UCSR0A |= (1<<TXC0); // clear transmit flag
}

/* get character from UART stream (blocking) */
char uart_getchar(FILE *stream)
{
	while (!(UCSR0A&(1<<RXC0))); // wait until data exists
	return UDR0;
}
