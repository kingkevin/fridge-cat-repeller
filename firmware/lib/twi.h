/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This library handles the Two-Wire Interface (TWI)/Inter-Integrated Circuit (I²C) configuration */

/* TWI/I2C clock speed, in Hz */
#define F_TWI 100000UL
/*
 * Maximal number of iterations to wait for a device to respond for a selection
 * Should be large enough to allow for a pending write to* complete, but low enough to properly abort an infinite loop in case a slave is broken or not present at all. 
 * With 100 kHz TWI clock, transfering the start condition and SLA+R/W packet takes about 10 µs.
 * The longest write period is supposed to not exceed ~ 10 ms.
 * Thus, normal operation should not require more than 100 iterations to get the device to respond to a selection.
 */
#define MAX_ITER 400
/* card (slave) address (7 MSB) */
#define SLA 0xA0

/* initialize 2-wire interface */
void twi_init(void);
/* read <len> bytes inyo <buf> using 2-wire interface, starting at <addr>
 * returns if it succeeded
 * 16 bits address is not supported
 */
bool twi_read_bytes(uint8_t addr, uint8_t len, uint8_t *buf);
/* write <len> bytes from <buf> on 2-wire interface starting at <addr>
 * returns if it successed
 * 16 bits address is not supported
 * ensure yourself the size of pages to be written
 */
bool twi_write_bytes(uint8_t addr, uint8_t len, uint8_t *buf);
