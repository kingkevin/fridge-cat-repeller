/* This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
/* This library handles the USART configuration */

/* set tolerance to 3% to allow 115200 baudrate with 16 MHz clock, else use 9600 for default <2% */
#define BAUD_TOL 3
/* serial baudrate, in bits per second (with 8N1 8 bits, not parity bit, 1 stop bit settings) */
#define BAUD 115200

/* input & output streams */
FILE uart_output;
FILE uart_input;
FILE uart_io;

/* configure serial port */
void uart_init(void);
/* put character on UART stream (blocking) */
void uart_putchar(char c, FILE *stream);
/* get character from UART stream (blocking) */
char uart_getchar(FILE *stream);

