This device is designed to repel cats.
In my case, it is used to scare it away from my fridge, before it opens and empties it.

It will first detect the presence and then emit ultrasounds.

This device uses an Arduino Nano 3.0 board with an Atmel ATmega328P micro-controller.
A HC-SR501 PIR motion detector in repeat mode is connected to 5V, GND, and D4.
When motion is detected (the cat is approaching) a light barrier is switched on.
A E18-D80NK is used as light barrier.
It is connected between 5V and D5.
The signal pin is connected to D6.
When the light barrier is broken (the cat is in front of the fridge) a loud (annoying) ultrasonic sound (24-25 kHz, outside the human hearing range) is emitted (using PWM).
To make noise a piezoelectric element (without oscillator) is connected between D9 and D10.
When the switch is released (the cat opened the fridge) an alarm is sounded (4-5kHz, audible by humans) until it the door (thus switch) closed.
A button switch is connected between GND and D3, and placed on the door of the fridge.

The firmware is available in the **firmware** directory.
To flash it, use `make`.
For a more verbose version, use `make debug` (after `make clean` if `make` has been used previously).
Debug messages are then sent over the serial port.
The code is well commented for more information.
